﻿using System;
using Gdk;

namespace Gtk.NotificationIcon
{
    class Program
    {
        // 托盘图标
        private static StatusIcon trayIcon;
        // 窗口
        private static Gtk.Window window;

        static void Main()
        {
            // 初始化GTK#
            Application.Init();

            // 创建一个带标题的窗口
            window = new Gtk.Window("Hello World");

            // 当窗口被关闭时，退出程序。
            window.DeleteEvent += delegate { Application.Quit(); };

            // 创建图标
            trayIcon = new StatusIcon(new Pixbuf("favicon.ico")) { Visible = true };

            // 当trayIcon被点击时，显示/隐藏窗口（甚至从面板/任务栏）。
            trayIcon.Activate += delegate { window.Visible = !window.Visible; };
            //当图标被右键点击时，显示一个弹出菜单。
            trayIcon.PopupMenu += OnTrayIconPopup;

            // 为图标提供一个工具提示
            trayIcon.Title = "Hello World Icon";

            // 显示主窗口并启动应用程序。
            window.ShowAll();
            Application.Run();
        }

        // 创建弹出菜单，右键点击。
        static void OnTrayIconPopup(object o, EventArgs args)
        {
            Menu popupMenu = new Menu();
            ImageMenuItem menuItemQuit = new ImageMenuItem("Quit");
            Gtk.Image appimg = new Gtk.Image(Stock.Quit, IconSize.Menu);
            menuItemQuit.Image = appimg;
            popupMenu.Add(menuItemQuit);
            // 点击quit后，退出应用程序。
            menuItemQuit.Activated += delegate { Application.Quit(); };
            popupMenu.ShowAll();
            popupMenu.Popup();
        }
    }
}
