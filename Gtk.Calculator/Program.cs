﻿using System;

namespace Gtk.Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Application.Init();
            var ui = new CalculatorView();
            ui.ShowAll();
            Application.Run();
        }
    }
}
