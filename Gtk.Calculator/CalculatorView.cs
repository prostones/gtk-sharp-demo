﻿using System;
using System.Collections.Generic;
using System.Text;
using Pango;

namespace Gtk.Calculator
{
    class CalculatorView : Window
    {
        private readonly Label _lblDisplay;
        public CalculatorView() : base("Gtk.Calculator")
        {
            //在窗体关闭时，退出应用程序
            this.DeleteEvent += CalculatorView_DeleteEvent;
            //设置窗体大小
            this.SetSizeRequest(300, 500);
            //设置窗体位置
            this.SetPosition(WindowPosition.Center);
            //去掉最大化按钮，不可以调整大小。
            this.Resizable = false;
            //创建一个 6 行 4 列的表格
            var table = new Table(6, 4, false);
            this.Add(table);
            //创建一个标签，用来显示结果
            _lblDisplay = new Label { Halign = Align.End, Margin = this.DefaultMargin, Valign = Align.End };
            table.Attach(_lblDisplay, 0, 4, 0, 1);

            //补充计算器的按钮
            //leftAttach,rightAttach,topAttach,bottomAttach 分别代表表中控件的左右上下边所在位置
            table.Attach(CreateButton("AC"), 0, 1, 1, 2);
            table.Attach(CreateButton("+/-"), 1, 2, 1, 2);
            table.Attach(CreateButton("%"), 2, 3, 1, 2);
            table.Attach(CreateButton("+"), 3, 4, 1, 2);

            table.Attach(CreateButton("7"), 0, 1, 2, 3);
            table.Attach(CreateButton("8"), 1, 2, 2, 3);
            table.Attach(CreateButton("9"), 2, 3, 2, 3);
            table.Attach(CreateButton("*"), 3, 4, 2, 3);

            table.Attach(CreateButton("4"), 0, 1, 3, 4);
            table.Attach(CreateButton("5"), 1, 2, 3, 4);
            table.Attach(CreateButton("6"), 2, 3, 3, 4);
            table.Attach(CreateButton("-"), 3, 4, 3, 4);

            table.Attach(CreateButton("1"), 0, 1, 4, 5);
            table.Attach(CreateButton("2"), 1, 2, 4, 5);
            table.Attach(CreateButton("3"), 2, 3, 4, 5);
            table.Attach(CreateButton("+"), 3, 4, 4, 5);


            table.Attach(CreateButton("0"), 0, 2, 5, 6);
            table.Attach(CreateButton("."), 2, 3, 5, 6);
            table.Attach(CreateButton("="), 3, 4, 5, 6);
        }
        /// <summary>
        /// 获取或设置 按钮之间的间距
        /// </summary>
        public int DefaultMargin { get; set; } = 3;

        private void CalculatorView_DeleteEvent(object o, DeleteEventArgs args)
        {
            Application.Quit();
        }
        private Button CreateButton(String text)
        {
            var btn = new Button(text) { Margin = DefaultMargin };
            btn.Clicked += Btn_Clicked;
            return btn;
        }

        private void Btn_Clicked(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                this._lblDisplay.Text += btn.Label;
            }
        }
    }
}
