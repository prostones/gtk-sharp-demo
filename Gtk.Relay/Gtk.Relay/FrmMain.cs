﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace Gtk.Relay
{
    public class FrmMain : Window
    {
        private readonly Table _layoutTable;
        private readonly String[] _serialPortNames;
        private ComboBox _cbSerialPorts;
        private SerialPort _serialPort;
        private RelayHelper _relayHelper;
        public FrmMain() : base("Gtk.Relay 继电器控制")
        {
            //推荐。在窗体被删除时退出程序。
            //否则，程序可能无法正确退出。
            this.DeleteEvent += (s, e) => { Application.Quit(); };
            //窗体的起始位置为屏幕正中心
            this.WindowPosition = WindowPosition.Center;
            //设置默认大小
            this.SetDefaultSize(800, 600);
            //去掉最大化按钮和通过鼠标调整大小功能
            this.Resizable = false;

            _serialPortNames = SerialPort.GetPortNames();

            //添加一个 4*4 的表格
            _layoutTable = new Table(4, 4, true);
            this.Add(_layoutTable);
            this.Line1();
            this.Line2();
            this.DeleteEvent += FrmMain_DeleteEvent;
        }

        private void FrmMain_DeleteEvent(object o, DeleteEventArgs args)
        {
            _serialPort?.Dispose();
        }

        private void Line1()
        {
            _cbSerialPorts = new ComboBox(_serialPortNames);
            _layoutTable.Attach(_cbSerialPorts, 0, 2, 0, 1);
            if (_serialPortNames.Any())
            {
                _cbSerialPorts.Model.IterNthChild(out var item, 0);
                _cbSerialPorts.SetActiveIter(item);

                var btn = new Button("打开串口");
                btn.Clicked += ToggleSerialPort_Clicked;
                _layoutTable.Attach(btn, 2, 4, 0, 1);
            }
        }

        private void Line2()
        {
            for (uint i = 0; i < 4; i++)
            {
                var cbItem = new CheckButton($"继电器{i + 1}");
                cbItem.Data["Index"] = i;
                cbItem.Clicked += CbItem_Clicked;
                _layoutTable.Attach(cbItem, i, i + 1, 1, 2);
            }
        }

        private void CbItem_Clicked(object sender, EventArgs e)
        {
            if (_relayHelper == null)
            {
                return;
            }
            if (sender is CheckButton cbItem)
            {
                var i = Convert.ToByte(cbItem.Data["Index"]);
                if (cbItem.Active)
                {
                    _relayHelper.Open(1, i);
                }
                else
                {
                    _relayHelper.Close(1, i);
                }
            }
        }

        private void ToggleSerialPort_Clicked(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                if (_serialPort == null || !_serialPort.IsOpen)
                {
                    _serialPort = new SerialPort(_serialPortNames[this._cbSerialPorts.Active], 9600)
                    {
                        ReceivedBytesThreshold = 8
                    };
                    _serialPort.Open();
                    _relayHelper = new RelayHelper(_serialPort);
                    btn.Label = "关闭串口";
                }
                else
                {
                    if (_serialPort != null)
                    {
                        if (_serialPort.IsOpen)
                        {
                            _serialPort.Close();
                        }
                        _serialPort.Dispose();
                        _serialPort = null;
                    }
                    btn.Label = "打开串口";
                }
            }
        }
    }
}
